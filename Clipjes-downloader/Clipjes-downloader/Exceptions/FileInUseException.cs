﻿using System;
using System.Runtime.Serialization;

namespace sommmen.clipjes_downloader.Exceptions
{
    internal class FileInUseException : Exception
    {
        public FileInUseException()
        {
        }

        public FileInUseException(string message) : base(message)
        {
        }

        public FileInUseException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected FileInUseException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}