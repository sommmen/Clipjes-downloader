﻿using System;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Net;
using sommmen.clipjes_downloader.Exceptions;
using sommmen.clipjes_downloader.Helpers;

namespace sommmen.clipjes_downloader.MyCookies
{
    /// <summary>
    ///     Class that can acces the Cookies.sqli file and retrieve cookies.
    /// </summary>
    internal class FirefoxCookieDatabase
    {
        public string DefaultDomain;
        public string DefaultHost;

        public CookieCollection GetAllCookiesOfHost()
        {
            return GetAllCookiesOfHost(DefaultDomain);
        }

        public CookieCollection GetAllCookiesOfHost(string host)
        {
            var path = GetProfileDirPath() + Path.DirectorySeparatorChar + "cookies.sqlite";

            //check if file is free, else throw exception
            if (FileHelper.IsFileLocked(new FileInfo(path)))
                throw new FileInUseException("File is locked");

            var result = new CookieCollection();
            using (var con = new SQLiteConnection("Data Source=" + path))
            using (
                var command =
                    new SQLiteCommand(
                        "SELECT name,value, host FROM moz_cookies WHERE host='" + (host ?? DefaultHost) + "'", con))
            {
                con.Open();
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                        result.Add(new Cookie((string) reader[0], (string) reader[1], "/", host ?? DefaultHost));
                }
            }
            return result;
        }

        /// <summary>
        ///     Gets the corrensponding profile path that contains the cookies.sqlite file.
        ///     keeps the 'FEBE' plugin in mind which changes the location.
        /// </summary>
        /// <returns>Path containing cookies.sqlite file</returns>
        private string GetProfileDirPath()
        {
            var profilesDir = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData),
                "Mozilla" + Path.DirectorySeparatorChar + "Firefox" + Path.DirectorySeparatorChar + "Profiles");
            return Directory.GetDirectories(profilesDir).Count(d => d.Contains("febeprof")) >= 1
                ? Directory.GetDirectories(profilesDir).First(d => d.Contains("febeprof"))
                : Directory.GetDirectories(profilesDir).First(d => d.Contains("default"));
        }

        #region constructors

        public FirefoxCookieDatabase(string defaultDomain, string defaultHost)
        {
            DefaultDomain = defaultDomain;
            DefaultHost = defaultHost;
        }

        public FirefoxCookieDatabase(string defaultDomain)
        {
            DefaultDomain = defaultDomain;
        }

        public FirefoxCookieDatabase()
        {
        }

        #endregion
    }
}