﻿using System;
using System.Linq;
using System.Net;
using Newtonsoft.Json;

namespace sommmen.clipjes_downloader
{
    internal class MyWebClient : WebClient
    {
        public CookieContainer CookieContainer;

        public MyWebClient()
        {
            CookieContainer = new CookieContainer(); //TODO improve with read/write actions from disk. (binaryformatter)
        }

        public Uri ResponseUri { get; private set; }

        public string GetCookieValue(string name)
        {
            return CookieContainer.GetCookies(ResponseUri).Cast<Cookie>().First(c => c.Name == name).Value;
        }

        public void AddCookieCollection(CookieCollection cookieCollection)
        {
            CookieContainer.Add(cookieCollection);
        }

        /// <summary>
        ///     Downloads as Json
        /// </summary>
        /// <typeparam name="T">Json type</typeparam>
        /// <param name="uri">Webpage uri</param>
        /// <returns>Result as Json in appropiate format</returns>
        public T DownloadJson<T>(Uri uri)
        {
            var str = DownloadString(uri);
            return JsonConvert.DeserializeObject<T>(str);
        }

        public T DownloadJson<T>(string url)
        {
            return DownloadJson<T>(new Uri(url));
        }

        protected override WebRequest GetWebRequest(Uri address)
        {
            var request = base.GetWebRequest(address);

            var webRequest = request as HttpWebRequest;
            if (webRequest != null)
                webRequest.CookieContainer = CookieContainer;

            return request;
        }

        protected override WebResponse GetWebResponse(WebRequest request)
        {
            var response = base.GetWebResponse(request);
            ResponseUri = response?.ResponseUri;
            return response;
        }
    }
}