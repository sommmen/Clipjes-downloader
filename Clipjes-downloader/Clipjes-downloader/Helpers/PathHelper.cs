﻿using System;
using System.IO;

namespace sommmen.clipjes_downloader.Helpers
{
    internal class PathHelper
    {
        /// <summary>
        ///     Sanitizes a string for use in paths. Removes illegal charactes.
        /// </summary>
        /// <param name="input">possibly dirty string</param>
        /// <returns>clean string</returns>
        public static string Santize(string input)
        {
            Array.ForEach(Path.GetInvalidFileNameChars(), c => input = input.Replace(c.ToString(), string.Empty));
            return input;
        }

        /// <summary>
        ///     BEWARE! UNTESTED
        ///     sanitizes an array of strings
        /// </summary>
        /// <param name="inputStrings">array of input strings</param>
        /// <returns>sanitized array of strings</returns>
        public static string[] Sanitize(string[] inputStrings)
        {
            Array.ForEach(inputStrings, s => Santize(s));
            return inputStrings;
        }
    }
}