﻿using System;
using System.Threading.Tasks;

namespace sommmen.clipjes_downloader.Helpers
{
    public static class RetryHelper
    {
        //TODO need to write an async logger.
        //private static ILog logger = LogManager.GetLogger(); //use a logger or trace of your choice

        //TODO delay doesnt seem to work properly
        public static bool RetryOnException(int times, TimeSpan delay, Action operation, bool doThrow)
        {
            var attempts = 0;
            do
            {
                try
                {
                    attempts++;
                    operation();
                    break; // Sucess! Lets exit the loop!
                }
                catch (Exception)
                {
                    if (attempts == times)
                        if (doThrow)
                            throw;
                        else
                            return false;
                    //logger.Error($"Exception caught on attempt {attempts} - will retry after delay {delay}", ex);

                    Task.Delay(delay).Wait();
                }
            } while (true);
            return true;
        }
    }
}