﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using sommmen.clipjes_downloader.JsonModels;

public enum State
{
    Idle,
    Downloading,
    Converting,
    //-------------------------
    //not used yet ;)
    Normalizing,
    StrippingSilence,
    Tagging,
    //-------------------------
    Unsuccessful,
    Done
}

namespace sommmen.clipjes_downloader
{
    /// <summary>
    ///     TODO refactor to singleton
    ///     Handles the console so it shows cool progress stuff.
    /// </summary>
    internal class ConsoleHandler
    {
        public const int Spacing = 25;


        public ConcurrentQueue<string> Messages;
        public ConcurrentDictionary<int, State> States; //TODO volatile still necesary here? i'm guessing not..

        public ConsoleHandler(List<Track> tracks)
        {
            Messages = new ConcurrentQueue<string>();
            Messages.Enqueue("Error messages will be revealed here:");
            TrackAmount = tracks.Count;
            States = new ConcurrentDictionary<int, State>();
            tracks.ForEach(track => States.TryAdd(track.Id, State.Idle));

            //we always want to see progress
        }

        private int TrackAmount { get; }

        public void Init()
        {
            var amtOfStates = Enum.GetNames(typeof(State)).Length;
            Console.Clear();
            for (var j = 0; j < amtOfStates * Spacing; j++)
                Console.Write("=");
            Console.WriteLine();

            var thread = new Thread(Run)
            {
                Priority = ThreadPriority.Highest
            };
            thread.Start();
        }

        private void Run()
        {
            var enumStates = Enum.GetNames(typeof(State));
            var cursorTop = Console.CursorTop;
            var messagescursor = cursorTop + 1;

            var messagesCopy = new Queue<string>();
            var statesCopy = new Dictionary<int, State>();
            do
            {
                //copy so we dont lock the dict while it's in use.
                statesCopy.Clear();
                statesCopy = States.ToDictionary(kvp => kvp.Key, kvp => kvp.Value);


                for (var index = 0; index < enumStates.Length; index++)
                {
                    var state = enumStates[index];
                    Console.SetCursorPosition(index * Spacing, cursorTop);

                    Console.Write(state + " : " + statesCopy.Count(s => Enum.GetName(typeof(State), s.Value) == state));
                }

                //process messages, first copy
                messagesCopy.Clear();
                while (!Messages.IsEmpty)
                {
                    string t;
                    Messages.TryDequeue(out t);
                    messagesCopy.Enqueue(t);
                }

                //then print
                while (messagesCopy.Count > 0)
                {
                    Console.SetCursorPosition(0, messagescursor);
                    Console.WriteLine(messagesCopy.Dequeue());
                    messagescursor++;
                }

                //stop if we reached the goal.
                if (statesCopy.Count(kv => kv.Value == State.Done || kv.Value == State.Unsuccessful) >= TrackAmount)
                    break;

                //delay
                var task = Task.Delay(1000);
                task.Wait();
            } while (true);
            Console.SetCursorPosition(enumStates.Length / 2 * Spacing, Console.CursorTop + 1);
            Console.WriteLine(">DONE<");
        }
    }
}