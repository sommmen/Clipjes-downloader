﻿using System;
using System.Collections.Specialized;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using NReco.VideoConverter;
using sommmen.clipjes_downloader.Exceptions;
using sommmen.clipjes_downloader.Helpers;
using sommmen.clipjes_downloader.JsonModels;
using sommmen.clipjes_downloader.MyCookies;
using sommmen.clipjes_downloader.Properties;
using VideoLibrary;

//_ga cookies are from google analytics.

//get a track's info by id.
//http://clipjes.nl/get-track/1454259

//get youtube link
//http://clipjes.nl/search-audio/Ed%20Sheeran/Shape%20of%20You  //%20 = +

//credit:
//https://github.com/Mavamaarten/YoutubeToMp3/blob/master/YouTubeDownloader/UI/frmMain.cs

//removing silence
//http://superuser.com/questions/323119/how-can-i-normalize-audio-using-ffmpeg
//use above link. sox seems a bit older:
//http://stackoverflow.com/questions/13043732/using-naudio-for-net-how-do-i-remove-the-silence-wave-at-the-end-of-mp3-file
// song to test: Firestone by kygo: http://clipjes.nl/track/218244

//ffmpeg progress bar? :D
//http://stackoverflow.com/questions/747982/can-ffmpeg-show-a-progress-bar
//https://gist.github.com/vitaLee/5055910
//http://stackoverflow.com/questions/1707516/c-sharp-and-ffmpeg-preferably-without-shell-commands
//https://www.codeproject.com/Tips/1021520/FFMPEG-Video-Converter-with-Progressbar-VB-NET

//get all songs
//http://clipjes.nl/user-library/get-all

//taglibsharp seems most viable..
//https://github.com/taglib/taglib

//jsonutils to convert json to c# classes. better than the 1st search result.
//http://jsonutils.com/

namespace sommmen.clipjes_downloader
{
    internal class Program
    {
        private static volatile FFMpegConverter _ffMpegConverter = new FFMpegConverter();
        private static ConsoleHandler _consoleHandler;

        private static void MainProg()
        {
            Console.SetWindowSize(ConsoleHandler.Spacing * Enum.GetNames(typeof(State)).Length, Console.WindowHeight * 2);

            var firefoxCookieDatabase = new FirefoxCookieDatabase("clipjes.nl", "Clipjes.nl");
            CookieCollection cookies;
            try
            {
                cookies = firefoxCookieDatabase.GetAllCookiesOfHost();
            }
            catch (FileInUseException)
            {
                Console.WriteLine("File is in use (by Firefox?) Close firefox? [Y/N]");
                if (Console.ReadKey(true).KeyChar.ToString().ToLower() != "y")
                    return;

                //close process
                var p = Process.GetProcessesByName("Firefox");
                foreach (var process in p)
                    process.CloseMainWindow();

                //wait for processes to exit
                foreach (var process in p)
                    process.WaitForExit();

                cookies = firefoxCookieDatabase.GetAllCookiesOfHost(); //TODO what if it goes wrong twice or more?
            }

            Clipjes clipjes = null;

            //retryhelper in case of connection failure.
            var succes = RetryHelper.RetryOnException(5, TimeSpan.FromSeconds(1.0d), () =>
            {
                using (var wc = new MyWebClient())
                {
                    //add cookies to get acces and fetch json data.
                    wc.AddCookieCollection(cookies);
                    clipjes = wc.DownloadJson<Clipjes>("http://clipjes.nl/user-library/get-all");
                }
            }, false);

            //error handling if we're not able to fetch the clipjes json
            if (!succes || clipjes == null)
            {
                Console.WriteLine("Could not retrieve songs link... Refresh session?");
                Console.ReadKey(true);
                return;
            }

            _consoleHandler = new ConsoleHandler(clipjes.Tracks.ToList());
            _consoleHandler.Init();
            Parallel.ForEach(clipjes.Tracks, track =>
            {
                var name = PathHelper.Santize(string.Join(", ", track.Artists.ToArray())) + " - " +
                           PathHelper.Santize(track.Name);
                var path = Settings.Default.resultDir + "\\" + name;
                if (!File.Exists(path + FileExtensions.Mp3))
                {
                    //could check if .mp4 exists here, but redownload in case of corruption is preferable.
                    _consoleHandler.States[track.Id] = State.Downloading;

                    //get the youtube id! TODO move to function
                    YoutubeTrack youtubeTrack = null;
                    try
                    {
                        succes = RetryHelper.RetryOnException(5, TimeSpan.FromSeconds(1.0d), () =>
                        {
                            using (var wc = new MyWebClient())
                            {
                                youtubeTrack =
                                    wc.DownloadJson<YoutubeTrack>("http://clipjes.nl/search-audio/" +
                                                                  track.Album.Artist.Name + "/" + track.Name);
                            }
                        }, true);
                    }
                    catch (Exception e)
                    {
                        _consoleHandler.Messages.Enqueue(e.GetType() + " : " + e.Message + "F: " + path);
                    }

                    //if we couldn't download the youtubeTrack
                    if (!succes || youtubeTrack == null)
                    {
                        _consoleHandler.States[track.Id] = State.Unsuccessful;
                        return;
                    }

                    //download the video
                    if (!DownloadVideo(youtubeTrack.Id, path))
                    {
                        _consoleHandler.States[track.Id] = State.Unsuccessful;
                        return;
                    }

                    //converting the file to .mp3
                    _consoleHandler.States[track.Id] = State.Converting;
                    if (!ConvertVideo(path))
                    {
                        _consoleHandler.States[track.Id] = State.Unsuccessful;
                        return;
                    }
                }

                _consoleHandler.States[track.Id] = State.Done;
                //TODO if !succes log to _unable
            });
        }

        private static bool DownloadVideo(string youtubeId, string path)
        {
            var uri = "https://youtube.com/watch?v=" + youtubeId;

            //get all streams of video
            var videoInfos = YouTube.Default.GetAllVideos(uri);

            try
            {
                //get video stream
                var video =
                    videoInfos.Where(_ => _.Format == VideoFormat.Mp4).OrderByDescending(_ => _.AudioBitrate).First();

                //write stream to disk
                File.WriteAllBytes(path + video.FileExtension, video.GetBytes());
            }
            catch (Exception e)
            {
                _consoleHandler.Messages.Enqueue(e.GetType() + " : " + e.Message + "F: " + path);
                return false;
            }
            return true;
        }

        private static bool ConvertVideo(string path)
        {
            try
            {
                //create new mp3 filestream to write to.
                var fileStream = new FileStream(path + FileExtensions.Mp3, FileMode.Create);

                //start ffmpeg converter. Only one program instance of ffmpeg is allowed, therefore we lock it.
                lock (_ffMpegConverter)
                {
                    _ffMpegConverter.ConvertMedia(path + FileExtensions.Mp4, "mp4", fileStream, "mp3",
                            new ConvertSettings {AudioCodec = "libmp3lame", CustomOutputArgs = "-q:a 0"});
                        //TODO optimize this (check normalize audio thread)
                }
                //delete mp4 file.
                File.Delete(path + FileExtensions.Mp4);
            }
            catch (Exception e)
            {
                _consoleHandler.Messages.Enqueue(e.GetType() + " : " + e.Message + "F: " + path);
                return false;
            }
            return true;
        }

        private static void Main(string[] args)
        {
            if (args.Length > 0)
                if (args[0] != null || args[0] != string.Empty)
                    Settings.Default.resultDir = args[0];

            Console.WriteLine("clipjes.nl downloader");

            MainProg();

            Console.ReadKey(true);
        }

        public static void PrintKeysAndValues(NameValueCollection myCol)
        {
            Console.WriteLine("   KEY        VALUE");
            foreach (var s in myCol.AllKeys)
                Console.WriteLine("   {0,-10} {1}", s, myCol[s]);
            Console.WriteLine();
        }
    }
}