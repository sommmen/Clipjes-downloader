﻿using Newtonsoft.Json;

namespace sommmen.clipjes_downloader.JsonModels
{
    public class Pivot
    {

        [JsonProperty("user_id")]
        public int UserId { get; set; }

        [JsonProperty("track_id")]
        public int TrackId { get; set; }

        [JsonProperty("created_at")]
        public string CreatedAt { get; set; }

        [JsonProperty("updated_at")]
        public string UpdatedAt { get; set; }
    }
}