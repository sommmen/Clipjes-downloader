﻿using Newtonsoft.Json;

namespace sommmen.clipjes_downloader.JsonModels
{
    public class YoutubeTrack
    {

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }
    }
}
