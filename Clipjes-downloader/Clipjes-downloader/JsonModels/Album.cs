﻿using Newtonsoft.Json;

namespace sommmen.clipjes_downloader.JsonModels
{
    public class Album
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("release_date")]
        public string ReleaseDate { get; set; }

        [JsonProperty("image")]
        public string Image { get; set; }

        [JsonProperty("artist_id")]
        public int ArtistId { get; set; }

        [JsonProperty("spotify_popularity")]
        public int? SpotifyPopularity { get; set; }

        [JsonProperty("fully_scraped")]
        public int FullyScraped { get; set; }

        [JsonProperty("artist")]
        public Artist Artist { get; set; }
    }
}