﻿using Newtonsoft.Json;

namespace sommmen.clipjes_downloader.JsonModels
{
    public class Artist
    {

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("spotify_followers")]
        public int? SpotifyFollowers { get; set; }

        [JsonProperty("spotify_popularity")]
        public int? SpotifyPopularity { get; set; }

        [JsonProperty("image_small")]
        public string ImageSmall { get; set; }

        [JsonProperty("image_large")]
        public string ImageLarge { get; set; }

        [JsonProperty("fully_scraped")]
        public int FullyScraped { get; set; }

        [JsonProperty("updated_at")]
        public string UpdatedAt { get; set; }

        [JsonProperty("bio")]
        public string Bio { get; set; }
    }
}