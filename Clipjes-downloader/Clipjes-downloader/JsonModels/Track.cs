using System.Collections.Generic;
using Newtonsoft.Json;

namespace sommmen.clipjes_downloader.JsonModels
{
    public class Track
    {

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("album_name")]
        public string AlbumName { get; set; }

        [JsonProperty("number")]
        public int Number { get; set; }

        [JsonProperty("duration")]
        public string Duration { get; set; }

        [JsonProperty("artists")]
        public IList<string> Artists { get; set; }

        [JsonProperty("youtube_id")]
        public object YoutubeId { get; set; }

        [JsonProperty("spotify_popularity")]
        public int SpotifyPopularity { get; set; }

        [JsonProperty("album_id")]
        public int AlbumId { get; set; }

        [JsonProperty("temp_id")]
        public string TempId { get; set; }

        [JsonProperty("url")]
        public object Url { get; set; }

        [JsonProperty("attached_at")]
        public string AttachedAt { get; set; }

        [JsonProperty("pivot")]
        public Pivot Pivot { get; set; }

        [JsonProperty("album")]
        public Album Album { get; set; }
    }
}