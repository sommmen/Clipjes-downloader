﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace sommmen.clipjes_downloader.JsonModels
{
    public class Clipjes
        {

            [JsonProperty("albums")]
            public IList<Album> Albums { get; set; }

            [JsonProperty("artists")]
            public IList<Artist> Artists { get; set; }

            [JsonProperty("tracks")]
            public IList<Track> Tracks { get; set; }
        }
}
