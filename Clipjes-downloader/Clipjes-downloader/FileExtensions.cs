﻿namespace sommmen.clipjes_downloader
{
    internal static class FileExtensions
    {
        public const string Mp3 = ".mp3";
        public const string Mp4 = ".mp4";
    }
}