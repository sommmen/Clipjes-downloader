This small app downloads all 'your songs' from clipjes.nl using FireFox cookies.

Then in parrallel (hard, but 10* quicker ;))
It proceeds to iterate this list, fetching all the youtube links.
And moving on to download all files (async, quite fast beware of a youtube ban (I don't have one yet, youtube can take some punches)).
It then converts to MP3 using Nrecosoft FMPG wrapper.
(in due time we will normalize the MP3 audio volume and remove silence)


Remarks about used libraries:
nreco VideoConverter:
- closed source
- limited documentation
- quite easy and almost perfect.
- Not thread safe, will simply throw an exception.
    I implemented a simple Lock on the converter class which worked beatifully.
- FFMPEG can't have more instances than 1, but then cause it's multithreaded already it does not have to.
    (ffmpeg can be run on 1 core check the docs, so you could also multithread it if you REALLY would like to)
Videlib
- YoutubeExtractor is dead.
- Fork the good pull request (fixes uri issues) else it won't work.



